import { herolist } from './list-heroes';

export class List {
   id: number;
   name: string;
   age: number;
   gender: string;
   marks: string;
   note: string;
   action: string;
  
}

