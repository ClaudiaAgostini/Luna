import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ModelsComponent } from './models/models.component';
import { ListComponent } from './list/list.component';
import { AlertComponent } from './alert/alert.component';
import { SrviceComponent } from './srvice/srvice.component';
import { RegisterComponent } from './register/register.component';
import { ModalComponent } from './modal/modal.component';
import { MatSliderModule } from '@angular/material/slider';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';  
import * as $ from "jquery";
import * as bootstrap from "bootstrap";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GridViewComponent } from './grid-view/grid-view.component';
 


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ModelsComponent,
    ListComponent,
    AlertComponent,
    SrviceComponent,
    RegisterComponent,
    ModalComponent,
    GridViewComponent
  ],
  
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatDialogModule,
    ReactiveFormsModule
   ],
  
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
