import { Component, OnInit } from '@angular/core';
import { Login } from '../logins';
import { List } from '../lists';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']

})

export class LoginComponent implements OnInit{
	
	submitted: false;
	heroform: FormGroup;
	
	user: Login ={
		username:'admin',
		password: 'password'
	};

constructor(private router: Router,	private FormBuilder: FormBuilder) {

}
 ngOnInit(): void{
 	this.heroform = this.FormBuilder.group({
 		username: ['', Validators.required],
 		inputpassword: ['', Validators.required]
 	});
 }

}
