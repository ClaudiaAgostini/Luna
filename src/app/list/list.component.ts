import { Component } from '@angular/core';
import { List } from '../lists';
import { herolist } from '../list-heroes';
import { enableProdMode } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatTableModule} from '@angular/material/table';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field'; 
import { MatInputModule } from '@angular/material/input';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {MatListModule} from '@angular/material/list';


enableProdMode();

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent {

  constructor(public dialog: MatDialog){}

  list = herolist;
  
  heroes: List[];

  dynamicArray: Array<List>=[];
  newList: any = {};

  ngOnInit(): void {
    this.newList = {name: "", age:"", gender: "", marks: "", note: "", action:""};
    this.dynamicArray.push(this.newList);
  }

  delete(index){
    if(this.dynamicArray.length == 1){
      return false;
    }
    else{
    this.dynamicArray.splice(index, 1);
    return true;
    }
  }

  openDialog(){
  	this.dialog.open(ListModal);
  } 
  
  }
 
@Component({
  selector: 'List-Modal',
  templateUrl: 'list-modal.component.html',
})

export class ListModal {}
