import { Router } from '@angular/router';

export interface Login{
	username: string;
	password: string;
}